const faker = require('faker');

faker.locale = 'pt_BR';

const clients = [];



for (let index = 0; index < 5; index++) {
  const name = faker.name.findName();
  const email = faker.internet.email(name);
  const phoneNumber = faker.phone.phoneNumber();
  const qtd = faker.random.number(20);
  const compra = faker.random.number(200);

  const birth = faker.date.past(109, 2019);
  const year = birth.toString().split(' ')[3];
  const age = 2019 - year;

  const dataC = faker.date.past(age, 2019);
  const anoCompra = dataC.toString().split(' ')[3];
  const ultimaCompra = 2019 - anoCompra;



  client = {
    name,
    email,
    phoneNumber,
    birth,
    ultimaCompra,
    qtd,
    compra,
    age,

  }
  clients.push(client);
}

//console.log(clients);

/***************************************** */
// 1. Desenvolva, utilizando filter , uma função que, dado um caractere 
//    de entrada, retorne todos os registros de clientes cujo o nome inicia com o caractere.
/*
const firstLetter = (letra) => clients.filter(value => value.name[0] === letra.toUpperCase())
console.log('----------------------------------------')
console.log('Clientes com a primeira letra ')
console.log(firstLetter('l'))
console.log('----------------------------------------')

*/
/*****************************************/
// 2. Retorne o array de clientes
/*
console.log("Array de Clientes");
console.log(clients)
console.log('----------------------------------------')
*/

/*************************************************** */
// 3. Desenvolva uma função que, dado o caractere de entrada, 
//    retorna apenas um número com o total de registros encontrados.
/*
const nFirstLetter = (letra) => clients.filter(value => value.name[0] === letra.toUpperCase());
console.log('----------------------------------------')
console.log('Numero de clientes com a primeira letra ')
//para verificar se a quantidade está correta:
//console.log(nFirstLetter('l'))
console.log(nFirstLetter('l').length)
console.log('----------------------------------------')
*/

/*************************************************** */
// 4. Desenvolva uma função que retorne apenas os nomes dos clientes.
/*
const nome = clients.map(value => value.name);
console.log("nome dos Clientes");
console.log(nome);
console.log('----------------------------------------')
*/

/*************************************************** */
// 5. Desenvolva uma função que retorne apenas o primeiro nome dos clientes.
/*
const nome = clients.map(value => value.name.split(" ")[0]);
console.log("nome dos Clientes");
console.log(nome);
console.log('----------------------------------------')
*/

/************************************************** */
//6. Desenvolva uma função que retorne apenas o primeiro nome dos 
//    clientes cujo os nomes começam com o caractere de entrada da função.
/*
const nome = clients
  .filter(value => value.name[0] === 'L')
  .map(value => value.name.split(" ")[0]);

console.log("nome dos Clientes");
console.log(nome);
console.log('----------------------------------------')
*/

/*************************************************** */
// 7. Desenvolva uma função que retorne todos os usuários que são maiores de idade.
/*
const nome = clients.filter(value => value.age >=18);
console.log("maiores de idade");
console.log(nome);
console.log('----------------------------------------')
*/

/*********************************************** */
// REVISAR 8. Desenvolva uma função que, dado um nome de entrada, retorna 
//    se o nome está contido na lista.
/*
console.log(clients);
nome= 'Lucas';
console.log(clients.map(value => value.name.split(" ")[0]).includes(nome) ? ("O nome está na lista") : ("O nome não está na lista"));
*/

/*********************************************** */
// 9. Implemente uma função que retorna o total de vendas realizadas 
//    somando as compras de todos os clientes.
/*
const total = clients
  .map(value => value.compra)
  .reduce((acc, next) => (acc+=next));
console.log("nome dos Clientes");
console.log(total);
console.log('----------------------------------------')
*/

/*************************************************** */
// 10. Implemente uma função que retorne os dados dos clientes 
//     que não compram há mais de 1 ano.
/*

const umAno = clients.filter(value => value.ultimaCompra > 1)
console.log('----------------------------------------')
console.log('Clientes que não compram a mais de um ano')
console.log(umAno)
console.log('----------------------------------------')
*/

/*********************************************** */
// 11. Implemente uma função que retorne os dados dos clientes que
//     já realizaram mais de 15 compras.
/*
const mais15 = clients.filter(value => value.qtd > 15)
console.log('----------------------------------------')
console.log('Clientes que não compram a mais de um ano')
console.log(mais15)
console.log('----------------------------------------')
/*
